package org.example.domain;

public class Sqrt implements IOperation {
    @Override
    public double calculator(double a, double fake) {
        return Math.sqrt(a);
    }
}
