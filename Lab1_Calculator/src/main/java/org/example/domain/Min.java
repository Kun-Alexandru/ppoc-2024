package org.example.domain;

public class Min implements IOperation {
    @Override
    public double calculator(double a, double b) {
        return Math.min(a, b);
    }
}
