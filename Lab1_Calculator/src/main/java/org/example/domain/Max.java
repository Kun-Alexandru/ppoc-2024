package org.example.domain;

public class Max implements IOperation {
    @Override
    public double calculator(double a, double b) {
        return Math.max(a, b);
    }
}
