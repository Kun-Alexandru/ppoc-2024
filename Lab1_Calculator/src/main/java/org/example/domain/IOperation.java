package org.example.domain;

public interface IOperation {
    double calculator(double a, double b);
}
