package org.example.domain;

public class Add implements IOperation {
    @Override
    public double calculator(double a, double b) {
        return a + b;
    }

}
