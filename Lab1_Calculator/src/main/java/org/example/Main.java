package org.example;

import org.example.ui.CalculatorUI;

public class Main {
    public static void main(String[] args) {
        CalculatorUI calculatorController = new CalculatorUI();
        calculatorController.run();

    }
}