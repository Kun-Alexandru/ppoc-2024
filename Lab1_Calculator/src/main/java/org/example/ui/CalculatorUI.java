package org.example.ui;

import org.example.service.CalculatorService;

import java.util.Scanner;

public class CalculatorUI {
    private final CalculatorService calculatorService;

    public CalculatorUI() {
        calculatorService = new CalculatorService();
    }

    public void run() {

        Scanner scanner = new Scanner(System.in);

        double total = 0.0;
        int counter = 0;

        while (true) {

            if (counter == 0) {

                System.out.print("First number: ");
                double a = 0;
                try {
                    a = scanner.nextDouble();
                } catch (Exception e) {
                    System.out.println("Error: " + e.getMessage());
                }

                System.out.print("Enter operator: ");
                String operator = scanner.next();

                if (operator.equals("exit")) {
                    break;
                }

                if (!operator.equals("sqrt")) {
                    System.out.print("Second number: ");
                }

                double b = 0;
                try {
                    if (operator.equals("sqrt")) {
                        b = -1;
                    } else {
                        b = scanner.nextDouble();
                    }
                } catch (Exception e) {
                    System.out.println("Error: " + e.getMessage());
                }

                try {
                    total = calculatorService.calculate(operator, a, b);
                    counter++;
                    System.out.println("Total: " + total);
                } catch (Exception e) {
                    System.out.println("Error: " + e.getMessage());
                }
            } else {
                System.out.print("Enter operation: ");
                String operator = scanner.next();

                if (operator.equals("exit")) {
                    break;
                }

                if (!operator.equals("sqrt")) {
                    System.out.print("Enter the number: ");
                }
                double b = 0;
                try {
                    if (operator.equals("sqrt")) {
                        b = -1;
                    } else {
                        b = scanner.nextDouble();
                    }
                } catch (Exception e) {
                    System.out.println("Error: " + e.getMessage());
                }

                try {
                    total = calculatorService.calculate(operator, total, b);
                    System.out.println("Total: " + total);
                } catch (Exception e) {
                    System.out.println("Error: " + e.getMessage());
                }
            }
        }
    }
}
