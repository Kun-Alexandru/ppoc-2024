package org.example.service;

import org.example.domain.*;

import java.util.Map;
import java.util.HashMap;

public class CalculatorService {
    private final Map<String, IOperation> operations = new HashMap<>();

    public CalculatorService(){
        operations.put("+", new Add());
        operations.put("-", new Subtraction());
        operations.put("*", new Multiplication());
        operations.put("/", new Division());
        operations.put("min", new Min());
        operations.put("max", new Max());
        operations.put("sqrt", new Sqrt());
    }

    public double calculate(String operator, double a, double b) {
        IOperation operation = operations.getOrDefault(operator, (x, y) -> {
            throw new IllegalArgumentException("Operation not existing!");
        });
        return operation.calculator(a, b);
    }
}
