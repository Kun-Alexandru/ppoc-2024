
import org.example.service.CalculatorService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CalculatorTests {
    private CalculatorService calculatorService;

    @BeforeEach
    void setUp() {
        calculatorService = new CalculatorService();
    }

    @Test
    void testAddition() {
        double result = calculatorService.calculate("+", 10.0, 7.0);
        assertEquals(17.0, result);
    }

    @Test
    void testSubtraction() {
        double result = calculatorService.calculate("-", 10.0, 3.0);
        assertEquals(7.0, result);
    }

    @Test
    void testMultiplication() {
        double result = calculatorService.calculate("*", 8.0, 4.0);
        assertEquals(32.0, result);
    }

    @Test
    void testDivision() {
        double result = calculatorService.calculate("/", 15.0, 5.0);
        assertEquals(3.0, result);
    }

    @Test
    void testMin() {
        double result = calculatorService.calculate("min", 9.0, 6.0);
        assertEquals(6.0, result);
    }

    @Test
    void testMax() {
        double result = calculatorService.calculate("max", 12.0, 8.0);
        assertEquals(12.0, result);
    }

    @Test
    void testSqrt() {
        double result = calculatorService.calculate("sqrt", 16.0, 0);
        assertEquals(4.0, result);
    }

    @Test
    void testUnknownOp() {
        assertThrows(IllegalArgumentException.class, () ->
                calculatorService.calculate("unknown", 5.0, 3.0));
    }

    @Test
    void testAdditionalAddition() {
        double result = calculatorService.calculate("+", 1.5, 2.5);
        assertEquals(4.0, result);
    }

    @Test
    void testAdditionalSubtraction() {
        double result = calculatorService.calculate("-", 7.0, 3.5);
        assertEquals(3.5, result);
    }

    @Test
    void testAdditionalMultiplication() {
        double result = calculatorService.calculate("*", 5.0, 2.0);
        assertEquals(10.0, result);
    }

    @Test
    void testAdditionalDivision() {
        double result = calculatorService.calculate("/", 12.0, 3.0);
        assertEquals(4.0, result);
    }

    @Test
    void testAdditionalMin() {
        double result = calculatorService.calculate("min", 3.0, 5.0);
        assertEquals(3.0, result);
    }

    @Test
    void testAdditionalMax() {
        double result = calculatorService.calculate("max", 7.0, 9.0);
        assertEquals(9.0, result);
    }

    @Test
    void testAdditionalSqrt() {
        double result = calculatorService.calculate("sqrt", 25.0, 0);
        assertEquals(5.0, result);
    }
}
