package org.example.repositories;

import com.koloboke.collect.set.hash.HashIntSet;
import com.koloboke.collect.set.hash.HashIntSets;

public class KolobokeHashIntSetRepository implements InMemoryRepository<Integer> {
    private final HashIntSet set = HashIntSets.newMutableSet();

    @Override
    public void add(Integer item) {
        set.add(item);
    }

    @Override
    public void remove(Integer item) {
        set.remove(item);
    }

    @Override
    public boolean contains(Integer item) {
        return set.contains(item);
    }

    @Override
    public void clear() {
        set.clear();
    }
}