package org.example.repositories;

import gnu.trove.list.array.TIntArrayList;

public class Trove4jIntArrayListRepository implements InMemoryRepository<Integer> {
    private final TIntArrayList array = new TIntArrayList();

    @Override
    public void add(Integer item) {
        array.add(item);
    }

    @Override
    public void remove(Integer item) {
        array.remove(item);
    }

    @Override
    public boolean contains(Integer item) {
        return array.contains(item);
    }

    @Override
    public void clear() {
        array.clear();
    }
}