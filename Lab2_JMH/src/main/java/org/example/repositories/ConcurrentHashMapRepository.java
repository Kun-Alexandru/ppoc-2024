package org.example.repositories;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapRepository<T> implements InMemoryRepository<T> {
    private final ConcurrentHashMap<T, Boolean> map = new ConcurrentHashMap<>();

    @Override
    public void add(T item) {
        map.put(item, true);
    }

    @Override
    public void remove(T item) {
        map.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return map.containsKey(item);
    }

    @Override
    public void clear(){
        map.clear();
    }
}
