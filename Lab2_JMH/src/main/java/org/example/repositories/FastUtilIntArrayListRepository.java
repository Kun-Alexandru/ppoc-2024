package org.example.repositories;

import it.unimi.dsi.fastutil.ints.IntArrayList;

import it.unimi.dsi.fastutil.ints.IntArrayList;

import java.util.Collection;

public class FastUtilIntArrayListRepository implements InMemoryRepository<Integer> {
    private IntArrayList arrayList = new IntArrayList();

    @Override
    public void add(Integer item) {
        arrayList.add(item);
    }

    @Override
    public void remove(Integer item) {
        arrayList.remove(item);
    }

    @Override
    public boolean contains(Integer item) {
        return arrayList.contains(item);
    }

    @Override
    public void clear(){
        arrayList.clear();
    }
}
