package org.example.repositories;

import java.util.ArrayList;
import java.util.List;

public class ArrayListRepository<T> implements InMemoryRepository<T> {
    private final List<T> list = new ArrayList<>();

    @Override
    public void add(T item) {
        list.add(item);
    }

    @Override
    public void remove(T item) {
        list.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return list.contains(item);
    }

    @Override
    public void clear(){
        list.clear();
    }
}
