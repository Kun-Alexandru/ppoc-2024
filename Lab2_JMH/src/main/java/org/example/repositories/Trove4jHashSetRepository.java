package org.example.repositories;

import gnu.trove.set.hash.THashSet;

public class Trove4jHashSetRepository<T> implements InMemoryRepository<T> {
    private final THashSet<T> set = new THashSet<>();

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void clear() {
        set.clear();
    }
}
