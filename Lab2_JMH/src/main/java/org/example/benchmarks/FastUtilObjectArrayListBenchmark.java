package org.example.benchmarks;

import org.example.domain.Order;
import org.example.repositories.FastUtilObjectArrayListRepository;
import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 10, time = 1)
@Fork(1)
public class FastUtilObjectArrayListBenchmark {
    @State(Scope.Benchmark)
    public static class BenchmarkState {
        FastUtilObjectArrayListRepository<Order> repository = new FastUtilObjectArrayListRepository<>();
        Order existingOrder = new Order(1, 33, 33);
        Order nonExistingOrder = new Order(6, 88, 88);

        public ArrayList<Order> list = new ArrayList<>();
        public int size = 5;

        @Setup(Level.Iteration)
        public void setup() {
            repository.clear();
            for(int i = 0; i < 100; i++){
                Order o = new Order(i+1,100,100);
                list.add(o);
            }
        }
    }

    @Benchmark
    public void add(BenchmarkState state)
    {
        for (int i = 0; i < state.list.size(); i++)
        {
            state.repository.add(state.list.get(i));
        }
    }

    @Benchmark
    public void remove(BenchmarkState state) {
        for (int i = 0; i < state.list.size(); i++)
        {
            state.repository.remove(state.list.get(i));
        }
    }

    @Benchmark
    public void contains(BenchmarkState state) {
        for (int i = 0; i < state.list.size(); i++)
        {
            if(i % 2 == 0)
                state.repository.contains(state.existingOrder);
            else
                state.repository.contains(state.nonExistingOrder);
        }
    }
}