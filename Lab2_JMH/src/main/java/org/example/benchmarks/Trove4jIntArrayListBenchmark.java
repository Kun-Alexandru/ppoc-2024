package org.example.benchmarks;

import org.example.domain.Order;
import org.example.repositories.Trove4jHashSetRepository;
import org.example.repositories.Trove4jIntArrayListRepository;
import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 10, time = 1)
@Fork(1)
public class Trove4jIntArrayListBenchmark {
    @State(Scope.Benchmark)
    public static class BenchmarkState {
        Trove4jIntArrayListRepository repository = new Trove4jIntArrayListRepository();
        public ArrayList<Integer> intList = new ArrayList<>();
        public Integer existingTestInteger;
        public Integer nonExistingTestInteger;

        public void setUp(){
            repository.clear();
            for(int i = 0; i < 100; i++){
                intList.add(i);
            }
            existingTestInteger = 33;
            nonExistingTestInteger = 100;
        }
    }

    @Benchmark
    public void add(BenchmarkState state)
    {
        for (int i = 0; i < state.intList.size(); i++)
        {
            state.repository.add(state.intList.get(i));
        }
    }

    @Benchmark
    public void remove(BenchmarkState state) {
        for (int i = 0; i < state.intList.size(); i++)
        {
            state.repository.remove(state.intList.get(i));
        }
    }

    @Benchmark
    public void contains(BenchmarkState state) {
        for (int i = 0; i < state.intList.size(); i++)
        {
            if  (i % 2 == 0)
                state.repository.contains(state.existingTestInteger);
            else
                state.repository.contains(state.nonExistingTestInteger);
        }
    }

}
