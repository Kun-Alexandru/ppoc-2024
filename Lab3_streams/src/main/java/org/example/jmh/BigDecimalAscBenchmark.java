package org.example.jmh;

import org.example.BigDecimalOperations;
import org.openjdk.jmh.annotations.*;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 2, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
public class BigDecimalAscBenchmark {
    @State(Scope.Benchmark)
    public static class MyState {
        BigDecimalOperations bigDecimalOperations = new BigDecimalOperations();
        List<BigDecimal> ascOrder;

        @Setup(Level.Iteration)
        public void setUp() {
            ascOrder = bigDecimalOperations.getRandomList(100000000);
            Collections.sort(ascOrder);
        }
    }
    @Benchmark
    public BigDecimal SumAsc(MyState state) {
        return state.bigDecimalOperations.getSum(state.ascOrder);
    }

    @Benchmark
    public BigDecimal AverageAsc(MyState state) {
        return state.bigDecimalOperations.getAverage(state.ascOrder);
    }

    @Benchmark
    public List<BigDecimal> top10PercentBiggestNumbersAsc(MyState state) throws Exception {
        return state.bigDecimalOperations.getTop10(state.ascOrder);
    }
}