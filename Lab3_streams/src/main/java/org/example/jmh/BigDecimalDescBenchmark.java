package org.example.jmh;

import org.example.BigDecimalOperations;
import org.openjdk.jmh.annotations.*;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 2, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
public class BigDecimalDescBenchmark {
    @State(Scope.Benchmark)
    public static class MyState {
        BigDecimalOperations bigDecimalOperations = new BigDecimalOperations();
        List<BigDecimal> descOrder;

        @Setup(Level.Iteration)
        public void setUp() {
            descOrder = bigDecimalOperations.getRandomList(100000000);
            descOrder.sort(Comparator.reverseOrder());
        }
    }
    @Benchmark
    public BigDecimal SumDesc(MyState state) {
        return state.bigDecimalOperations.getSum(state.descOrder);
    }

    @Benchmark
    public BigDecimal AverageDesc(MyState state) {
        return state.bigDecimalOperations.getAverage(state.descOrder);
    }

    @Benchmark
    public List<BigDecimal> top10PercentBiggestNumbersDesc(MyState state) throws Exception {
        return state.bigDecimalOperations.getTop10(state.descOrder);
    }
}