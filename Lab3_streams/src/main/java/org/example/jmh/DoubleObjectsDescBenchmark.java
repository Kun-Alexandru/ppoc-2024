package org.example.jmh;

import org.example.DoubleObjectOperations;
import org.openjdk.jmh.annotations.*;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 2, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
public class DoubleObjectsDescBenchmark {
    @State(Scope.Benchmark)
    public static class MyState {
        DoubleObjectOperations doubleObjectOperations = new DoubleObjectOperations();
        List<Double> descOrder;

        @Setup(Level.Trial)
        public void setUp() {
            descOrder = doubleObjectOperations.getRandomList(100000000);
            descOrder.sort(Comparator.reverseOrder());
        }
    }
    @Benchmark
    public Double SumDesc(MyState state) {
        return state.doubleObjectOperations.getSum(state.descOrder);
    }

    @Benchmark
    public Double AverageDesc(MyState state) {
        return state.doubleObjectOperations.getAverage(state.descOrder);
    }

    @Benchmark
    public List<Double> top10PercentBiggestNumbersDesc(MyState state) throws Exception {
        return state.doubleObjectOperations.getTop10(state.descOrder);
    }

}
