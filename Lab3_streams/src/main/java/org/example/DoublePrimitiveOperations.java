package org.example;

import java.util.Arrays;
import java.util.Random;

public class DoublePrimitiveOperations {
    public double[] getRandomList(int size) {
        double[] doublesArray = new double[size];
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            doublesArray[i] = random.nextDouble();
        }
        return doublesArray;
    }

    public double getSum(double[] doublesArray) {
        return Arrays.stream(doublesArray).sum();
    }

    public double getAverage(double[] doublesArray) {
        double sum = getSum(doublesArray);
        return sum / doublesArray.length;
    }

    public double[] getTop10(double[] doublesArray) {
        int size = doublesArray.length;
        return Arrays.stream(doublesArray)
                .sorted()
                .skip(size - size / 10)
                .toArray();
    }
}

