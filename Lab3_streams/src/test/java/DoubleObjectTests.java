import org.example.DoubleObjectOperations;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DoubleObjectTests {
    DoubleObjectOperations doubleObjectOperations;

    @BeforeEach
    void setUp() {
        doubleObjectOperations = new DoubleObjectOperations();
    }

    @Test
    void generateRandomDoubleList() {
        int size = 100;
        List<Double> generatedDoubles = doubleObjectOperations.getRandomList(size);
        Assertions.assertEquals(size, generatedDoubles.size());
        for (Double d : generatedDoubles) {
            Assertions.assertNotNull(d);
        }
    }

    @Test
    void computeSum() {
        List<Double> doubles = Arrays.asList(10.5, 20.3, 5.2);
        Double expectedSum = 36.0;
        Double actualSum = doubleObjectOperations.getSum(doubles);
        Assertions.assertEquals(expectedSum, actualSum);
    }

    @Test
    void computeAverage() {
        List<Double> doubles = Arrays.asList(10.5, 20.3, 5.2);
        Double expectedAvg = 12.0;
        Double actualAvg = doubleObjectOperations.getAverage(doubles);
        Assertions.assertEquals(expectedAvg, actualAvg);
    }

    @Test
    void top10() {
        List<Double> doubles = Arrays.asList(12.5, 17.3, 8.9, 25.1, 19.7, 11.0, 15.8, 22.6, 14.3, 10.2, 16.5);
        List<Double> top10 = new ArrayList<>();
        try {
            top10 = doubleObjectOperations.getTop10(doubles);
        } catch (Exception ignored) {
        }

        Double expected = 25.1;
        Assertions.assertTrue(top10.contains(expected));
    }
}
