import org.example.BigDecimalOperations;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BigDecimalTests {
    BigDecimalOperations bigDecimalOperations;

    @BeforeEach
    void setUp() {
        bigDecimalOperations = new BigDecimalOperations();
    }

    @Test
    void generateRandomBigDecimalList() {
        int size = 100;
        List<BigDecimal> generatedBigDecimals = bigDecimalOperations.getRandomList(size);
        Assertions.assertEquals(size, generatedBigDecimals.size());
        for (BigDecimal bd : generatedBigDecimals) {
            Assertions.assertNotNull(bd);
        }
    }

    @Test
    void computeSum() {
        List<BigDecimal> bigDecimals = Arrays.asList(new BigDecimal("15.75"),
                new BigDecimal("42.8"),
                new BigDecimal("7.91"));
        BigDecimal expectedSum = new BigDecimal("66.46");
        BigDecimal actualSum = bigDecimalOperations.getSum(bigDecimals);
        Assertions.assertEquals(expectedSum, actualSum);
    }

    @Test
    void computeAverage() {
        List<BigDecimal> bigDecimals = Arrays.asList(new BigDecimal("15.75"),
                new BigDecimal("42.8"),
                new BigDecimal("7.91"));
        BigDecimal expectedAvg = new BigDecimal("22.15");
        BigDecimal actualAvg = bigDecimalOperations.getAverage(bigDecimals);
        Assertions.assertEquals(expectedAvg, actualAvg);
    }

    @Test
    void printTop10PercentBiggestNumbers() {
        List<BigDecimal> bigDecimals = Arrays.asList(new BigDecimal("14.2"),
                new BigDecimal("19.6"),
                new BigDecimal("10.1"),
                new BigDecimal("27.3"),
                new BigDecimal("21.4"),
                new BigDecimal("12.0"),
                new BigDecimal("16.9"),
                new BigDecimal("23.8"),
                new BigDecimal("15.3"),
                new BigDecimal("11.0"),
                new BigDecimal("18.5"));
        List<BigDecimal> top10 = new ArrayList<>();
        try {
            top10 = bigDecimalOperations.getTop10(bigDecimals);
        } catch (Exception ignored) {
        }

        BigDecimal bigDec = new BigDecimal("27.3");

        Assertions.assertTrue(top10.contains(bigDec));
    }
}
